﻿using System;
using System.Collections.Generic;
using System.Text;
using VFIREWOOD.Services.Interfaces.Models;

namespace VFIREWOOD.Services.Interfaces
{
    public interface ISendEmail
    {
        void SendEmail(ViewOrder viewOrder, string recipientEmail, string templatePath);
    }
}
