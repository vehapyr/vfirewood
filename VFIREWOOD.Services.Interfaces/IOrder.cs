﻿using System;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Services.Interfaces.Models;

namespace VFIREWOOD.Services.Interfaces
{
    public interface IOrder
    {
        Task<ViewOrder> MakeOrder(string jsonprod, Order order);
    }
}
