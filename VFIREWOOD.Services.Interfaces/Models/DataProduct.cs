﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFIREWOOD.Services.Interfaces.Models
{
    public class DataProduct
    {
        public int ProductId { get; set; }
        public int Amount { get; set; }
    }
}
