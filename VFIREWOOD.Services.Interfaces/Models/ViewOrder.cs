﻿using System;
using System.Collections.Generic;
using System.Text;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Services.Interfaces.Models
{
    public class ViewOrder
    {
        public ViewOrder()
        {
            Order = new Order();
            Check = new Check();
            CheckItems = new List<CheckItem>();
            Products = new List<Product>();
        }

        public Order Order { get; set; }
        public Check Check { get; set; }
        public List<CheckItem> CheckItems { get; set; }
        public List<Product> Products { get; set; }
    }
}
