﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class ProductRepository : IProductRepository
    {
        private readonly string connectionString;
        public ProductRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public IEnumerable<Product> GetProductList()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Product>("SELECT * FROM Products");
            }
        }

        public IEnumerable<Product> GetProductBySubCategoryId(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Product>("SELECT * FROM Products WHERE SubCategoryId = @id", new { id });
            }
        }

        public IEnumerable<Product> GetProductBySubCategoryName(string name)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Product>("SELECT * FROM Products WHERE SubCategoryName = @name", new { name });
            }
        }

        public IEnumerable<Product> GetProductsByListIds(IEnumerable<int> listIds)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Product>("SELECT * FROM Products WHERE Id IN @ids", new { ids = listIds });
            }
        }

        public IEnumerable<Product> GetDiscountedProduct()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Product>("SELECT * FROM Products WHERE Discount != 0");
            }
        }

        public T GetProduct<T>(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<T>("SELECT * FROM Products WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public IEnumerable<Product> GetProductsByName(string productName)
        {
            productName = productName.ToLower();
            List<Product> resultProducts = new List<Product>();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                List<Product> products = db.Query<Product>("SELECT * FROM Products").ToList();
                foreach (var product in products)
                {
                    if (product.Name.ToLower().Contains(productName))
                    {
                        resultProducts.Add(product);
                    }
                }
            }
            return resultProducts;
        }

        //public object GetProductById(int id)
        //{
        //    using (IDbConnection db = new SqlConnection(connectionString))
        //    {
        //        object product = new object();
        //        var categoryId = db.Query<int>("SELECT CategoryId FROM Products WHERE Id = @id", new { id }).FirstOrDefault();
        //        if (categoryId != 0)
        //        {
        //            CategoryRepository categoryRepository = new CategoryRepository();
        //            var category = categoryRepository.GetCategory(categoryId);

        //            if (category.Name == "Computers")
        //                product = GetProduct<Computer>(id);
        //            else if (category.Name == "Laptops")
        //                product = GetProduct<Laptop>(id);
        //            else if (category.Name == "GameConsole")
        //                product = GetProduct<GameConsole>(id);
        //            else if (category.Name == "Monitors")
        //                product = GetProduct<Monitor>(id);
        //            else if (category.Name == "Keyboards")
        //                product = GetProduct<Keyboard>(id);
        //            else if (category.Name == "Mouses")
        //                product = GetProduct<Mouse>(id);
        //        }
        //        return product;
        //    }
        //}

        public Product GetProductById(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Product>("SELECT * FROM Products WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public void Create(Product item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO Products (Name, Price, Discount, Dimensions, Unit, SubCategoryName, SubCategoryId)" +
                    "VALUES(@Name, @Price, @Discount, @Dimensions, @Unit, @SubCategoryName, @SubCategoryId); SELECT CAST(SCOPE_IDENTITY() as int)";
                db.Execute(sqlQuery, item);
                // int? userId = db.Query<int>(sqlQuery, item).FirstOrDefault();  получение id добавленного продукта
            }
        }

        public void InsertRange(IEnumerable<Product> products)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO Products (Name, Price, Discount, Dimensions, Unit, SubCategoryName, SubCategoryId)" +
                    "VALUES(@Name, @Price, @Discount, @Dimensions, @Unit, @SubCategoryName, @SubCategoryId); SELECT CAST(SCOPE_IDENTITY() as int)";
                db.Execute(sqlQuery, products);
                // int? userId = db.Query<int>(sqlQuery, item).FirstOrDefault();  получение id добавленного продукта
            }
        }

        public void Update(Product item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Products SET Name = @Name, Price = @Price, Discount = @Discount, Dimensions = @Dimensions, Unit = @Unit, SubCategoryName = @SubCategoryName, SubCategoryId = @SubCategoryId" +
                    " WHERE Id = @Id";
                db.Execute(sqlQuery, item);
            }
        }

        public void UpdateRange(List<Product> products)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Products SET Name = @Name, Price = @Price, Discount = @Discount, Dimensions = @Dimensions, Unit = @Unit, SubCategoryName = @SubCategoryName, SubCategoryId = @SubCategoryId" +
                    " WHERE Id = @Id";
                db.Execute(sqlQuery, products);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM Products WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
