﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class CheckItemRepository : ICheckItemRepository
    {
        private readonly string connectionString;
        public CheckItemRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public IEnumerable<CheckItem> GetCheckItemList()
        {
            List<CheckItem> checkItems = new List<CheckItem>();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                checkItems = db.Query<CheckItem>("SELECT * FROM CheckItems").ToList();
            }
            return checkItems;
        }

        public CheckItem GetCheckItem(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<CheckItem>("SELECT * FROM CheckItems WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public IEnumerable<CheckItem> GetCheckItemsByCheckId(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<CheckItem>("SELECT * FROM CheckItems WHERE CheckId = @id", new { id });
            }
        }

        public IEnumerable<CheckItem> GetCheckItemsByCheckIds(IEnumerable<int> listIds)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<CheckItem>("SELECT * FROM CheckItems WHERE CheckId IN @ids", new { ids = listIds });
            }
        }

        public void Create(CheckItem item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO CheckItems (Amount, Price, Sum, ProductId, CheckId) VALUES(@Amount, @Price, @Sum, @ProductId, @CheckId)";
                db.Execute(sqlQuery, item);
            }
        }

        public void InsertRange(IEnumerable<CheckItem> checkItems)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO CheckItems (Amount, Price, Sum, ProductId, CheckId) VALUES(@Amount, @Price, @Sum, @ProductId, @CheckId)";
                db.Execute(sqlQuery, checkItems);
            }
        }

        public void Update(CheckItem item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE CheckItems SET Amount = @Amount, Price = @Price, Sum = @Sum, ProductId = @ProductId, CheckId = @CheckId WHERE Id = @Id";
                db.Execute(sqlQuery, item);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM CheckItems WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
