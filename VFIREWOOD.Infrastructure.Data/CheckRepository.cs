﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class CheckRepository : ICheckRepository
    {
        private readonly string connectionString;
        public CheckRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public IEnumerable<Check> GetCheckList()
        {
            List<Check> checks = new List<Check>();
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                checks = db.Query<Check>("SELECT * FROM Checks").ToList();
            }
            return checks;
        }

        public IEnumerable<Check> GetChecksByOrderIds(IEnumerable<int> listIds)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Check>("SELECT * FROM Checks WHERE Id IN @ids", new { ids = listIds });
            }
        }

        public Check GetCheck(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Check>("SELECT * FROM Checks WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public int Create(Check item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO Checks (TotalSum, DatePurchase) VALUES(@TotalSum, @DatePurchase); SELECT CAST(SCOPE_IDENTITY() as int)";
                int checkId = db.Query<int>(sqlQuery, item).First();
                return checkId;
            }
        }

        public void Update(Check item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Checks SET TotalSum = @TotalSum, DatePurchase = @DatePurchase WHERE Id = @Id";
                db.Execute(sqlQuery, item);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM Checks WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
