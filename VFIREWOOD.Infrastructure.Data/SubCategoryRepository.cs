﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class SubCategoryRepository : ISubCategoryRepository
    {
        private readonly string connectionString;
        public SubCategoryRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public IEnumerable<SubCategory> GetSubCategoryList()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<SubCategory>("SELECT * FROM SubCategories").ToList();
            }
        }

        public IEnumerable<SubCategory> GetSubCategoryByCategoryId(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<SubCategory>("SELECT * FROM SubCategories WHERE CategoryId = @id", new { id }).ToList();
            }
        }

        public IEnumerable<SubCategory> GetSubCategoryByCategoryName(string name)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<SubCategory>("SELECT * FROM SubCategories WHERE CategoryName = @name", new { name }).ToList();
            }
        }

        public SubCategory GetSubCategory(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<SubCategory>("SELECT * FROM SubCategories WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public SubCategory GetSubCategoryByName(string name)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<SubCategory>("SELECT * FROM SubCategories WHERE Name = @name", new { name }).FirstOrDefault();
            }
        }

        public int Create(SubCategory item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                //var sqlQuery = "INSERT INTO SubCategories (Name, RuName, Description, ImageBase64, CategoryName, CategoryId, BottomDescription) VALUES(@Name, @RuName, @Description, @ImageBase64, @CategoryName, @CategoryId, @BottomDescription)";
                //db.Execute(sqlQuery, item);

                var sqlQuery = "INSERT INTO SubCategories (Name, RuName, Description, ImageBase64, CategoryName, CategoryId, BottomDescription) VALUES(@Name, @RuName, @Description, @ImageBase64, @CategoryName, @CategoryId, @BottomDescription); SELECT CAST(SCOPE_IDENTITY() as int)";
                int id = db.Query<int?>(sqlQuery, item).FirstOrDefault() ?? 0;
                return id;
            }
        }
        public int Update(SubCategory item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE SubCategories SET Name = @Name, RuName = @RuName, Description = @Description, ImageBase64 = @ImageBase64, CategoryName = @CategoryName, CategoryId = @CategoryId, BottomDescription = @BottomDescription WHERE Id = @Id";
                return db.Execute(sqlQuery, item);
            }
        }
        public int Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM SubCategories WHERE Id = @id";
                return db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
