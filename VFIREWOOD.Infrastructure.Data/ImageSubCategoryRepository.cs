﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;

namespace VFIREWOOD.Infrastructure.Data
{
    public class ImageSubCategoryRepository : IImageSubCategory
    {
        private readonly string connectionString;
        public ImageSubCategoryRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public ImageSubCategory GetImageSubCategory(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<ImageSubCategory>("SELECT * FROM ImageSubCategories WHERE SubCategoryId = @id", new { id }).FirstOrDefault();
            }
        }

        public IEnumerable<ImageSubCategory> GetImageSubCategoryById(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<ImageSubCategory>("SELECT * FROM ImageSubCategories WHERE SubCategoryId = @id", new { id });
            }
        }

        public void InsertRange(IEnumerable<ImageSubCategory> imageSubCategorys)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO ImageSubCategories (ImageBase64, SubCategoryId) VALUES(@ImageBase64, @SubCategoryId)";
                db.Execute(sqlQuery, imageSubCategorys);
            }
        }

        public void DeleteRange(IEnumerable<int> ids)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM ImageSubCategories WHERE Id in @ids";
                db.Execute(sqlQuery, new { ids });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
