﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class AdvertisingRepository : IAdvertisingRepository
    {
        private readonly string connectionString;
        public AdvertisingRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public Advertising GetAdvertising(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Advertising>("SELECT * FROM Advertisings WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public IEnumerable<Advertising> GetAdvertisingList()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Advertising>("SELECT * FROM Advertisings").ToList();
            }
        }

        public int Create(Advertising item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO Advertisings (Text, Link, ImageBase64, Show) VALUES(@Text, @Link, @ImageBase64, @Show); SELECT CAST(SCOPE_IDENTITY() as int)";
                //db.Execute(sqlQuery, item);
                int id = db.Query<int?>(sqlQuery, item).FirstOrDefault() ?? 0;
                return id;
            }
        }

        public int Update(Advertising item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Advertisings SET Text = @Text, Link = @Link, ImageBase64 = @ImageBase64, Show = @Show WHERE Id = @Id";
                return db.Execute(sqlQuery, item);
            }
        }

        public int Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM Advertisings WHERE Id = @id";
                return db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
