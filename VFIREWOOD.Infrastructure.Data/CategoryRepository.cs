﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class CategoryRepository : ICategoryRepository
    {     
        private readonly string connectionString;
        public CategoryRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public IEnumerable<Category> GetCategoryList()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Category>("SELECT * FROM Categories").ToList();
            }
        }
        public Category GetCategory(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Category>("SELECT * FROM Categories WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }
        public Category GetCategoryByName(string name)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Category>("SELECT * FROM Categories WHERE Name = @name", new { name }).FirstOrDefault();
            }
        }
        public int Create(Category item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO Categories (Name, RuName, ImageBase64) VALUES(@Name, @RuName, @ImageBase64); SELECT CAST(SCOPE_IDENTITY() as int)";
                //db.Execute(sqlQuery, item);
                int id = db.Query<int?>(sqlQuery, item).FirstOrDefault() ?? 0;             
                return id;
            }
        }
        public int Update(Category item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Categories SET Name = @Name, RuName = @RuName, ImageBase64 = @ImageBase64 WHERE Id = @Id";
                return db.Execute(sqlQuery, item);
            }
        }
        public int Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM Categories WHERE Id = @id";
                return db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
