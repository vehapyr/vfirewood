﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Domain.Interfaces;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace VFIREWOOD.Infrastructure.Data
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string connectionString;
        public OrderRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        public IEnumerable<Order> GetOrderList()
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Order>("SELECT * FROM Orders");
            }
        }

        public IEnumerable<Order> GetOrdersByUserId(string id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Order>("SELECT * FROM Orders WHERE BuyerId = @id", new { id });
            }
        }

        public Order GetOrder(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                return db.Query<Order>("SELECT * FROM Orders WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public int Create(Order item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "INSERT INTO Orders (Fio, Email, PhoneNumber, Address, Comment, IsCompany, CheckId) " +
                    "VALUES(@Fio, @Email, @PhoneNumber, @Address, @Comment, @IsCompany, @CheckId); SELECT CAST(SCOPE_IDENTITY() as int)";
                int orderId = db.Query<int>(sqlQuery, item).First();
                return orderId;
            }
        }

        public void Update(Order item)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "UPDATE Orders SET Fio = @Fio, Email = @Email, PhoneNumber = @PhoneNumber," +
                    "Address = @Address, Comment = @Comment, IsCompany = @IsCompany, CheckId = @CheckId" +
                    " WHERE Id = @Id";
                db.Execute(sqlQuery, item);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(connectionString))
            {
                var sqlQuery = "DELETE FROM Orders WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (IDbConnection db = new SqlConnection(connectionString))
                    {
                        db.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
