﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface IAdvertisingRepository : IDisposable
    {
        IEnumerable<Advertising> GetAdvertisingList();
        Advertising GetAdvertising(int id);
        int Create(Advertising item);
        int Update(Advertising item);
        int Delete(int id);
    }
}
