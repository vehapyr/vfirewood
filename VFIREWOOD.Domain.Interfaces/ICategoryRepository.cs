﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface ICategoryRepository : IDisposable
    {
        IEnumerable<Category> GetCategoryList();
        Category GetCategory(int id);
        Category GetCategoryByName(string name);
        int Create(Category item);
        int Update(Category item);
        int Delete(int id);
    }
}
