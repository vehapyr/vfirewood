﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface ICheckItemRepository : IDisposable
    {
        IEnumerable<CheckItem> GetCheckItemList();
        CheckItem GetCheckItem(int id);
        IEnumerable<CheckItem> GetCheckItemsByCheckId(int id);
        IEnumerable<CheckItem> GetCheckItemsByCheckIds(IEnumerable<int> listIds);
        void Create(CheckItem item);
        void InsertRange(IEnumerable<CheckItem> checkItems);
        void Update(CheckItem item);
        void Delete(int id);
    }
}
