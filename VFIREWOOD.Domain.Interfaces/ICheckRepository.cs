﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface ICheckRepository : IDisposable
    {
        IEnumerable<Check> GetCheckList();
        IEnumerable<Check> GetChecksByOrderIds(IEnumerable<int> listIds);
        Check GetCheck(int id);
        int Create(Check item);
        void Update(Check item);
        void Delete(int id);
    }
}
