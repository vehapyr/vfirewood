﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface IOrderRepository : IDisposable
    {
        IEnumerable<Order> GetOrderList();
        Order GetOrder(int id);
        int Create(Order item);
        void Update(Order item);
        void Delete(int id);
    }
}
