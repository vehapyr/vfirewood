﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface ISubCategoryRepository : IDisposable
    {
        IEnumerable<SubCategory> GetSubCategoryList();
        IEnumerable<SubCategory> GetSubCategoryByCategoryId(int id);
        IEnumerable<SubCategory> GetSubCategoryByCategoryName(string name);
        SubCategory GetSubCategory(int id);
        SubCategory GetSubCategoryByName(string name);
        int Create(SubCategory item);
        int Update(SubCategory item);
        int Delete(int id);
    }
}
