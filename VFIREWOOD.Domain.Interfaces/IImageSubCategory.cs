﻿using System;
using System.Collections.Generic;
using System.Text;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface IImageSubCategory : IDisposable
    {
        ImageSubCategory GetImageSubCategory(int id);
        IEnumerable<ImageSubCategory> GetImageSubCategoryById(int id);
        void InsertRange(IEnumerable<ImageSubCategory> imageSubCategorys);
        void DeleteRange(IEnumerable<int> ids);
    }
}
