﻿using System;
using System.Collections.Generic;
using VFIREWOOD.Domain.Core;

namespace VFIREWOOD.Domain.Interfaces
{
    public interface IProductRepository : IDisposable
    {
        IEnumerable<Product> GetProductList();
        IEnumerable<Product> GetProductBySubCategoryId(int id);
        IEnumerable<Product> GetProductBySubCategoryName(string name);
        IEnumerable<Product> GetProductsByListIds(IEnumerable<int> listIds);
        IEnumerable<Product> GetDiscountedProduct();
        T GetProduct<T>(int id);
        //object GetProductById(int id);
        Product GetProductById(int id);
        void Create(Product item);
        void InsertRange(IEnumerable<Product> products);
        void Update(Product item);
        void Delete(int id);
    }
}
