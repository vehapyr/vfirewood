﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;

namespace VFIREWOOD.Services.Data
{
    public class OrderService
    {
        OrderRepository repository;

        public OrderService(IConfiguration configuration)
        {
            repository = new OrderRepository(configuration);
        }

        public async Task<IEnumerable<Order>> GetOrderList()
        {
            return await Task.Run(() =>
            {
                return repository.GetOrderList();
            });
        }

        public async Task<IEnumerable<Order>> GetOrdersByUserId(string id)
        {
            return await Task.Run(() =>
            {
                return repository.GetOrdersByUserId(id);
            });
        }

        public async Task<int> Create(Order item)
        {
            return await Task.Run(() =>
            {
                return repository.Create(item);
            });
        }

        public async Task<Order> GetOrder(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetOrder(id);
            });
        }

        public async void Update(Order item)
        {
            await Task.Run(() =>
            {
                repository.Update(item);
            });
        }

        public async void Delete(int id)
        {
            await Task.Run(() =>
            {
                repository.Delete(id);
            });
        }
    }
}
