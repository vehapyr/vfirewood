﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;
using System.Linq;

namespace VFIREWOOD.Services.Data
{
    public class SubCategoryService
    {
        SubCategoryRepository repository;
        IMemoryCache _memoryCache;
        public SubCategoryService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            repository = new SubCategoryRepository(configuration);
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<SubCategory>> GetSubCategoryList()
        {
            return await Task.Run(() =>
            {
                List<SubCategory> subCategories = new List<SubCategory>();
                if (!_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                {
                    subCategories = repository.GetSubCategoryList().ToList();
                    if (subCategories.Count() != 0)
                    {
                        _memoryCache.Set("AllSubCategories", subCategories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(160)));
                    }
                }
                return subCategories;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetSubCategoryList();
            //});
        }

        public async Task<IEnumerable<SubCategory>> GetSubCategoryByCategoryId(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetSubCategoryByCategoryId(id);
            });
        }

        public async Task<IEnumerable<SubCategory>> GetSubCategoryByCategoryName(string name)
        {
            return await Task.Run(() =>
            {
                List<SubCategory> subCategories = new List<SubCategory>();
                if (_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                    return subCategories.Where(x => x.CategoryName == name).ToList();
                return repository.GetSubCategoryByCategoryName(name);
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetSubCategoryByCategoryName(name);
            //});
        }

        public async Task<SubCategory> GetSubCategory(int id)
        {
            return await Task.Run(() =>
            {
                List<SubCategory> subCategories = new List<SubCategory>();
                if (_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                    return subCategories.FirstOrDefault(x => x.Id == id);
                return repository.GetSubCategory(id);
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetSubCategory(id);
            //});
        }

        public async Task<SubCategory> GetSubCategoryByName(string name)
        {
            return await Task.Run(() =>
            {
                List<SubCategory> subCategories = new List<SubCategory>();
                if (_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                    return subCategories.FirstOrDefault(x => x.Name == name);
                return repository.GetSubCategoryByName(name);
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetSubCategoryByName(name);
            //});
        }

        public async Task<int> Create(SubCategory item)
        {
            return await Task.Run(() =>
            {
                int id = repository.Create(item);
                if (id != 0)
                {
                    item.Id = id;
                    List<SubCategory> subCategories = new List<SubCategory>();
                    if (_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                    {
                        subCategories.Add(item);
                    }
                    else
                    {
                        subCategories = new List<SubCategory>();
                        subCategories.Add(item);
                    }
                    _memoryCache.Set("AllSubCategories", subCategories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(160)));
                }
                return id;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.Create(item);
            //});
        }

        public async Task Update(SubCategory item)
        {
            await Task.Run(() =>
            {
                var result = repository.Update(item);
                if (result != 0)
                {
                    List<SubCategory> subCategories = new List<SubCategory>();
                    if (_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                    {
                        foreach (var sub in subCategories.Where(x => x.Id == item.Id))
                        {
                            sub.Name = item.Name;
                            sub.RuName = item.RuName;
                            sub.Description = item.Description;
                            sub.BottomDescription = item.BottomDescription;
                            sub.ImageBase64 = item.ImageBase64;
                            sub.CategoryName = item.CategoryName;
                            sub.CategoryId = sub.CategoryId;                       
                            break;
                        }
                    }
                    else
                    {
                        subCategories = new List<SubCategory>();
                        subCategories.Add(item);
                    }
                    _memoryCache.Set("AllSubCategories", subCategories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(160)));
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Update(item);
            //});
        }

        public async Task Delete(int id)
        {
            await Task.Run(() =>
            {
                var result = repository.Delete(id);
                if (result != 0)
                {
                    List<SubCategory> subCategories = new List<SubCategory>();
                    if (_memoryCache.TryGetValue("AllSubCategories", out subCategories))
                    {
                        var subCategory = subCategories.FirstOrDefault(x => x.Id == id);
                        subCategories.Remove(subCategory);
                        if (subCategories.Count != 0)
                            _memoryCache.Set("AllSubCategories", subCategories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(160)));
                        else
                            _memoryCache.Remove("AllSubCategories");
                    }
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Delete(id);
            //});
        }
    }
}
