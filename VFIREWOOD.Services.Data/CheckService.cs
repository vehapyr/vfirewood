﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;

namespace VFIREWOOD.Services.Data
{
    public class CheckService
    {
        CheckRepository repository;

        public CheckService(IConfiguration configuration)
        {
            repository = new CheckRepository(configuration);
        }

        public async Task<IEnumerable<Check>> GetChecksByOrderIds(IEnumerable<int> listIds)
        {
            return await Task.Run(() =>
            {
                return repository.GetChecksByOrderIds(listIds);
            });
        }

        public async Task<int> Create(Check item)
        {
            return await Task.Run(() =>
            {
                return repository.Create(item);
            });
        }

        public async Task<Check> GetCheck(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetCheck(id);
            });
        }
    }
}
