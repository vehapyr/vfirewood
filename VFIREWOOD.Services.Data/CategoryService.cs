﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;

namespace VFIREWOOD.Services.Data
{
    public class CategoryService
    {
        CategoryRepository repository;
        IMemoryCache _memoryCache;

        public CategoryService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            repository = new CategoryRepository(configuration);
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<Category>> GetCategoryList()
        {
            return await Task.Run(() =>
            {
                List<Category> categories = new List<Category>();
                if (!_memoryCache.TryGetValue("AllCategories", out categories))
                {
                    categories = repository.GetCategoryList().ToList();
                    if (categories.Count() != 0)
                    {
                        _memoryCache.Set("AllCategories", categories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(180)));
                    }
                }
                return categories;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetCategoryList();
            //});
        }

        public async Task<Category> GetCategory(int id)
        {
            return await Task.Run(() =>
            {
                List<Category> categories = new List<Category>();
                Category category = new Category();
                if (_memoryCache.TryGetValue("AllCategories", out categories))
                    category = categories.FirstOrDefault(x => x.Id == id);
                else
                    category = repository.GetCategory(id);
                return category;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetCategory(id);
            //});
        }

        public async Task<Category> GetCategoryByName(string name)
        {
            return await Task.Run(() =>
            {
                List<Category> categories = new List<Category>();
                Category category = new Category();
                if (_memoryCache.TryGetValue("AllCategories", out categories))
                    category = categories.FirstOrDefault(x => x.Name == name);
                else
                    category = repository.GetCategoryByName(name);
                return category;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetCategoryByName(name);
            //});
        }

        public async Task<int> Create(Category item)
        {
            return await Task.Run(() =>
            {
                int id = repository.Create(item);
                if (id != 0)
                {
                    item.Id = id;
                    List<Category> categories = new List<Category>();
                    if (_memoryCache.TryGetValue("AllCategories", out categories))
                    {
                        categories.Add(item);
                    }
                    else
                    {
                        categories = new List<Category>();
                        categories.Add(item);
                    }
                    _memoryCache.Set("AllCategories", categories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(180)));
                }
                return id;
            });

            //await Task.Run(() =>
            //{
            //    repository.Create(item);
            //});
        }

        public async Task Update(Category item)
        {
            await Task.Run(() =>
            {
                var result = repository.Update(item);
                if (result != 0)
                {
                    List<Category> categories = new List<Category>();
                    if (_memoryCache.TryGetValue("AllCategories", out categories))
                    {
                        foreach (var cat in categories.Where(x=>x.Id == item.Id))
                        {
                            cat.Name = item.Name;
                            cat.RuName = item.RuName;
                            cat.SubCategories = item.SubCategories;
                            cat.ImageBase64 = item.ImageBase64;
                            break;
                        }
                    }
                    else
                    {
                        categories = new List<Category>();
                        categories.Add(item);
                    }
                    _memoryCache.Set("AllCategories", categories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(180)));
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Update(item);
            //});
        }

        public async Task Delete(int id)
        {
            await Task.Run(() =>
            {
                var result = repository.Delete(id);
                if (result != 0)
                {
                    List<Category> categories = new List<Category>();
                    if (_memoryCache.TryGetValue("AllCategories", out categories))
                    {
                        var category = categories.FirstOrDefault(x => x.Id == id);
                        categories.Remove(category);
                        if (categories.Count != 0)
                            _memoryCache.Set("AllCategories", categories, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(180)));
                        else
                            _memoryCache.Remove("AllCategories");
                    }
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Delete(id);
            //});
        }
    }
}
