﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;

namespace VFIREWOOD.Services.Data
{
    public class ProductService
    {
        ProductRepository repository;

        public ProductService(IConfiguration configuration)
        {
            repository = new ProductRepository(configuration);
        }

        public async Task Create(Product item)
        {
            await Task.Run(() =>
            {
                repository.Create(item);
            });
        }

        public async Task InsertRange(List<Product> products)
        {
            await Task.Run(() =>
            {
                repository.InsertRange(products);
            });
        }

        public async Task Update(Product item)
        {
            await Task.Run(() =>
            {
                repository.Update(item);
            });
        }

        public async Task UpdateRange(List<Product> products)
        {
            await Task.Run(() =>
            {
                repository.UpdateRange(products);
            });
        }

        public async Task Delete(int id)
        {
            await Task.Run(() =>
            {
                repository.Delete(id);
            });
        }

        public async Task<IEnumerable<Product>> GetProductList()
        {
            return await Task.Run(() =>
            {
                return repository.GetProductList();
            });
        }

        //public async Task<T> GetProduct<T>(int id)
        //{
        //    return await Task.Run(() =>
        //    {
        //        return repository.GetProduct<T>(id);
        //    });
        //}

        public async Task<Product> GetProductById(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetProductById(id);
            });
        }

        public async Task<IEnumerable<Product>> GetProductsByName(string productName)
        {
            return await Task.Run(() =>
            {
                return repository.GetProductsByName(productName);
            });
        }

        public async Task<IEnumerable<Product>> GetProductsByListIds(IEnumerable<int> listIds)
        {
            return await Task.Run(() =>
            {
                return repository.GetProductsByListIds(listIds);
            });
        }

        public async Task<IEnumerable<Product>> GetProductBySubCategoryId(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetProductBySubCategoryId(id);
            });
        }

        public async Task<IEnumerable<Product>> GetProductBySubCategoryName(string name)
        {
            return await Task.Run(() =>
            {
                return repository.GetProductBySubCategoryName(name);
            });
        }

        public async Task<IEnumerable<Product>> GetDiscountedProduct()
        {
            return await Task.Run(() =>
            {
                return repository.GetDiscountedProduct();
            });
        }

        public IEnumerable<T> GetOffsetProduct<T>(int page, out int maxPage, IEnumerable<T> listProducts)
        {
            int pageSize = 10;

            int count = listProducts.Count();
            maxPage = (count / pageSize) + (count % pageSize > 0 ? 1 : 0);

            return listProducts.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<T> SortProductsByNovelty<T>(IEnumerable<T> products)
        {
            return products.OrderByDescending(x => ((dynamic)x).Id);
        }

        public IEnumerable<T> SortProductsByCheap<T>(IEnumerable<T> products)
        {
            return products.OrderBy(x => ((dynamic)x).Price);
        }

        public IEnumerable<T> SortProductsByExpensive<T>(IEnumerable<T> products)
        {
            return products.OrderByDescending(x => ((dynamic)x).Price);
        }

        //public object GetProductById(int id)
        //{
        //    return repository.GetProductById(id);
        //}
    }
}
