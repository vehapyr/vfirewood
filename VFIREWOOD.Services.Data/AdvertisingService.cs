﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;
using System.Linq;

namespace VFIREWOOD.Services.Data
{
    public class AdvertisingService
    {
        AdvertisingRepository repository;
        IMemoryCache _memoryCache;
        public AdvertisingService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            repository = new AdvertisingRepository(configuration);
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<Advertising>> GetAdvertisingList()
        {
            return await Task.Run(() =>
            {
                List<Advertising> advertising = new List<Advertising>();
                if (!_memoryCache.TryGetValue("AllAdvertising", out advertising))
                {
                    advertising = repository.GetAdvertisingList().ToList();
                    if (advertising.Count() != 0)
                    {
                        _memoryCache.Set("AllAdvertising", advertising, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(300)));
                    }
                }
                return advertising;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetAdvertisingList();
            //});
        }

        public async Task<Advertising> GetAdvertising(int id)
        {
            return await Task.Run(() =>
            {
                List<Advertising> advertisings = new List<Advertising>();
                Advertising advertising = new Advertising();
                if (_memoryCache.TryGetValue("AllAdvertising", out advertisings))
                    advertising = advertisings.FirstOrDefault(x => x.Id == id);
                else
                    advertising = repository.GetAdvertising(id);
                return advertising;
            });

            //return await Task.Run(() =>
            //{
            //    return repository.GetAdvertising(id);
            //});
        }

        public async Task Create(Advertising item)
        {
            await Task.Run(() =>
            {
                int id = repository.Create(item);
                if (id != 0)
                {
                    item.Id = id;
                    List<Advertising> advertisings = new List<Advertising>();
                    if (_memoryCache.TryGetValue("AllAdvertising", out advertisings))
                    {
                        advertisings.Add(item);
                    }
                    else
                    {
                        advertisings = new List<Advertising>();
                        advertisings.Add(item);
                    }
                    _memoryCache.Set("AllAdvertising", advertisings, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(300)));
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Create(item);
            //});
        }

        public async Task Update(Advertising item)
        {
            await Task.Run(() =>
            {
                var result = repository.Update(item);
                if (result != 0)
                {
                    List<Advertising> advertisings = new List<Advertising>();
                    if (_memoryCache.TryGetValue("AllAdvertising", out advertisings))
                    {
                        foreach (var adv in advertisings.Where(x => x.Id == item.Id))
                        {
                            adv.Text = item.Text;
                            adv.Link = item.Link;
                            adv.ImageBase64 = item.ImageBase64;
                            adv.Show = item.Show;
                            break;
                        }
                    }
                    else
                    {
                        advertisings = new List<Advertising>();
                        advertisings.Add(item);
                    }
                    _memoryCache.Set("AllAdvertising", advertisings, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(300)));
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Update(item);
            //});
        }

        public async Task Delete(int id)
        {
            await Task.Run(() =>
            {
                var result = repository.Delete(id);
                if (result != 0)
                {
                    List<Advertising> advertisings = new List<Advertising>();
                    if (_memoryCache.TryGetValue("AllAdvertising", out advertisings))
                    {
                        var advertising = advertisings.FirstOrDefault(x => x.Id == id);
                        advertisings.Remove(advertising);
                        if (advertisings.Count != 0)
                            _memoryCache.Set("AllAdvertising", advertisings, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(300)));
                        else
                            _memoryCache.Remove("AllAdvertising");
                    }
                }
            });

            //await Task.Run(() =>
            //{
            //    repository.Delete(id);
            //});
        }
    }
}
