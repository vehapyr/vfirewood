﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;

namespace VFIREWOOD.Services.Data
{
    public class ImageSubCategoryService
    {
        ImageSubCategoryRepository repository;

        public ImageSubCategoryService(IConfiguration configuration)
        {
            repository = new ImageSubCategoryRepository(configuration);
        }

        public async Task<ImageSubCategory> GetImageSubCategory(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetImageSubCategory(id);
            });
        }

        public async Task<IEnumerable<ImageSubCategory>> GetImageSubCategoryById(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetImageSubCategoryById(id);
            });
        }

        public async Task InsertRange(IEnumerable<ImageSubCategory> imageSubCategorys)
        {
            await Task.Run(() =>
            {
                repository.InsertRange(imageSubCategorys);
            });
        }

        public async Task DeleteRange(IEnumerable<int> ids)
        {
            await Task.Run(() =>
            {
                repository.DeleteRange(ids);
            });
        }
    }
}
