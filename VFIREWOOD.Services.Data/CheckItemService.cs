﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;

namespace VFIREWOOD.Services.Data
{
    public class CheckItemService
    {
        CheckItemRepository repository;

        public CheckItemService(IConfiguration configuration)
        {
            repository = new CheckItemRepository(configuration);
        }

        public async void Create(CheckItem item)
        {
            await Task.Run(() =>
            {
                repository.Create(item);
            });
        }

        public async Task<IEnumerable<CheckItem>> GetCheckItemsByCheckId(int id)
        {
            return await Task.Run(() =>
            {
                return repository.GetCheckItemsByCheckId(id);
            });
        }

        public async Task<IEnumerable<CheckItem>> GetCheckItemsByCheckIds(IEnumerable<int> listIds)
        {
            return await Task.Run(() =>
            {
                return repository.GetCheckItemsByCheckIds(listIds);
            });
        }

        public async Task InsertRange(List<CheckItem> checkItems)
        {
            await Task.Run(() =>
            {
                repository.InsertRange(checkItems);
            });
        }
    }
}
