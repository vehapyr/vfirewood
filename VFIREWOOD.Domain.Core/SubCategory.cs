﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class SubCategory
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Английское название")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Русское название")]
        public string RuName { get; set; }
        [Display(Name = "Верхние описание")]
        public string Description { get; set; }
        [Display(Name = "Нижнее описание")]
        public string BottomDescription { get; set; }
        public string ImageBase64 { get; set; }
        [Required]
        [Display(Name = "Название категории")]
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
    }
}
