﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VFIREWOOD.Domain.Core
{
    public class Category
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Английское название")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Русское название")]
        public string RuName { get; set; }
        public string ImageBase64 { get; set; }
        public List<SubCategory> SubCategories { get; set; }
    }
}
