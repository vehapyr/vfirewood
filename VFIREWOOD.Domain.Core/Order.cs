﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class Order
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "ФИО")]
        [StringLength(100, MinimumLength = 10)]
        [RegularExpression(@"^[а-яА-Я\s]*$", ErrorMessage = "Используйте только буквы")]
        public string Fio { get; set; }
        [Required]
        [Display(Name = "Почта")]
        [StringLength(30)]
        [EmailAddress(ErrorMessage = "Не корректный email")]
        public string Email { get; set; }
        [Required]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Номер телефона должен состоять из 11 цифер")]
        [Display(Name = "Номер телефона")]
        [Phone(ErrorMessage = "Введите номер телефона")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Номер телефона должен состоять из цифер")]
        public string PhoneNumber { get; set; }
        [StringLength(150)]
        [Display(Name = "Адрес доставки")]
        public string Address { get; set; }
        [StringLength(300, ErrorMessage = "Максильная длинна 300 символов")]
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }
        [Display(Name = "Вы компания?")]
        public bool IsCompany { get; set; }
        public int CheckId { get; set; }
    }
}
