﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class Advertising
    {
        public int Id { get; set; }
        [Display(Name = "Текст")]
        public string Text { get; set; }
        [Required]
        [Display(Name = "Ссылка")]
        public string Link { get; set; }
        public string ImageBase64 { get; set; }
        [Display(Name = "Отображается?")]
        public bool Show { get; set; }
    }
}
