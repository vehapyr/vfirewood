﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class Check
    {
        public int Id { get; set; }
        public decimal TotalSum { get; set; }
        public DateTime DatePurchase { get; set; }
    }
}
