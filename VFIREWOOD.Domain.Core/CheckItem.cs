﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class CheckItem
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public decimal Sum { get; set; }
        public int ProductId { get; set; }
        public int CheckId { get; set; }
    }
}
