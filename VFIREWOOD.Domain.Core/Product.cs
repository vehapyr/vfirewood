﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Русское название")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }
        [Required]
        [Display(Name = "Скидка")]
        public decimal Discount { get; set; }
        [Required]
        [Display(Name = "Размеры")]
        public string Dimensions { get; set; }
        [Required]
        [Display(Name = "Единица измерения")]
        public string Unit { get; set; }
        [Required]
        [Display(Name = "Название подкатегории")]
        public string SubCategoryName { get; set; }
        public int SubCategoryId { get; set; }
    }
}
