﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFIREWOOD.Domain.Core
{
    public class ImageSubCategory
    {
        public int Id { get; set; }
        public string ImageBase64 { get; set; }
        public int SubCategoryId { get; set; }
    }
}
