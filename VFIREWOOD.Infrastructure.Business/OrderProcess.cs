﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VFIREWOOD.Services.Interfaces;
using VFIREWOOD.Services.Interfaces.Models;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Services.Data;
using System.Linq;

namespace VFIREWOOD.Infrastructure.Business
{
    public class OrderProcess : IOrder
    {
        private ProductService _productService;
        private OrderService _orderService;
        private CheckService _checkService;
        private CheckItemService _checkItemService;
        public OrderProcess(ProductService productService, OrderService orderService, CheckService checkService, CheckItemService checkItemService)
        {
            _productService = productService;
            _orderService = orderService;
            _checkService = checkService;
            _checkItemService = checkItemService;
        }

        public async Task<ViewOrder> MakeOrder(string jsonprod, Order order)
        {
            List<string> jsonDeserialize = jsonprod.Split(':', '|').ToList();
            jsonDeserialize.RemoveAll(x => x == string.Empty);
            List<string> json = new List<string>();
            foreach (var item in jsonDeserialize)
            {
                string result = new string(item.Where(x => char.IsDigit(x)).ToArray());
                json.Add(result);
            }

            List<DataProduct> dataProducts = new List<DataProduct>();
            for (int i = 0; i < json.Count; i++)
            {
                DataProduct data = new DataProduct();
                data.ProductId = int.Parse(json[i]);
                data.Amount = int.Parse(json[++i]);
                dataProducts.Add(data);
            }

            ViewOrder viewOrder = new ViewOrder();
            List<CheckItem> checkItems = new List<CheckItem>();
            List<Product> listProductsViewOrder = new List<Product>();

            var listIds = dataProducts.Select(x => x.ProductId);
            var products = (List<Product>)await _productService.GetProductsByListIds(listIds);

            if (products.Count != 0)
            {
                foreach (var data in dataProducts)
                {
                    CheckItem checkItem = new CheckItem();

                    foreach (var product in products)
                    {
                        if (product.Id == data.ProductId)
                        {
                            checkItem.ProductId = product.Id;
                            checkItem.Amount = data.Amount;
                            checkItem.Price = product.Discount == 0 ? product.Price : product.Discount;
                            checkItem.Sum = checkItem.Price * data.Amount;
                            checkItems.Add(checkItem);

                            listProductsViewOrder.Add(product);
                            break;
                        }
                    }
                }
            }

            if (listProductsViewOrder.Count != 0)
                await _productService.UpdateRange(listProductsViewOrder);

            if (checkItems.Count > 0)
            {
                Check check = new Check();
                check.DatePurchase = DateTime.Now;
                check.TotalSum = checkItems.Sum(x => x.Sum);

                int checkId = await _checkService.Create(check);

                foreach (var item in checkItems)
                    item.CheckId = checkId;

                await _checkItemService.InsertRange(checkItems);

                order.CheckId = checkId;
                order.Id = await _orderService.Create(order);

                viewOrder.Order = order;
                viewOrder.Check = check;
                viewOrder.CheckItems = checkItems;
                viewOrder.Products = listProductsViewOrder;

                return viewOrder;
            }
            return null;
        }
    }
}
