﻿using MimeKit;
using MailKit.Net.Smtp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VFIREWOOD.Services.Interfaces;
using VFIREWOOD.Services.Interfaces.Models;

namespace VFIREWOOD.Infrastructure.Business
{
    public class EmailService : ISendEmail
    {
        public void SendEmail(ViewOrder viewOrder, string recipientEmail, string templatePath)
        {
            Task.Run(async () =>
            {
                InternetAddressList list = new InternetAddressList();
                list.Add(new MailboxAddress("FaneraLes-Torg.ru", "info@fanerales-torg.ru"));
                list.Add(new MailboxAddress("FaneraLes-Torg.ru", recipientEmail));

                var emailMessage = new MimeMessage();

                emailMessage.From.Add(new MailboxAddress("FaneraLes-Torg.ru", "info@fanerales-torg.ru"));
                emailMessage.To.AddRange(list);
                emailMessage.Subject = "Заказ № " + viewOrder.Order.Id;

                string template = System.IO.File.ReadAllText(System.IO.Path.Combine(templatePath, "YourOrderTemplate.html"));

                string IsCompany = "Нет";
                if (viewOrder.Order.IsCompany)
                    IsCompany = "Да";

                template = template.Replace("{{OrderNumber}}", viewOrder.Order.Id.ToString());
                template = template.Replace("{{IsCompany}}", IsCompany);
                template = template.Replace("{{Fio}}", viewOrder.Order.Fio.ToString());
                template = template.Replace("{{Email}}", viewOrder.Order.Email.ToString());
                template = template.Replace("{{PhoneNumber}}", viewOrder.Order.PhoneNumber.ToString());
                template = template.Replace("{{DatePurchase}}", viewOrder.Check.DatePurchase.ToString());

                if (viewOrder.Order.Address != null)
                    template = template.Replace("{{Address}}", $"<tr><td>Адрес</td><td>{viewOrder.Order.Address}</td></tr>");
                else
                    template = template.Replace("{{Address}}", string.Empty);

                if (viewOrder.Order.Comment != null)
                    template = template.Replace("{{Comment}}", $"<tr><td>Комментарий</td><td>{viewOrder.Order.Comment}</td></tr>");
                else
                    template = template.Replace("{{Comment}}", string.Empty);

                string items = string.Empty;

                for (int i = 0; i < viewOrder.CheckItems.Count; i++)
                {
                    items += "<tr>" +

                            $"<td>{viewOrder.Products[i].Name} {viewOrder.Products[i].Dimensions}</td>" +
                            $"<td style='display: none;'></td>" +
                            $"<td>{viewOrder.Products[i].Price} руб</td>" +
                            $"<td>{viewOrder.CheckItems[i].Amount}</td>" +
                            $"<td>{viewOrder.CheckItems[i].Sum} руб</td>" +
                            "</tr>";
                }

                template = template.Replace("{{DataCheckItem}}", items);
                template = template.Replace("{{TotalSum}}", viewOrder.Check.TotalSum.ToString());

                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = template
                };

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("mail.hosting.reg.ru", 25, false);
                    await client.AuthenticateAsync("info@fanerales-torg.ru", "HnRp*53?");
                    await client.SendAsync(emailMessage);

                    await client.DisconnectAsync(true);
                }
            });    
        }
    }
}