﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Services.Data;
using VFIREWOOD.Services.Interfaces.Models;

namespace VFIREWOOD.Areas.Management.Controllers
{
    [Authorize(Roles = "admin, employee")]
    [Area("Management")]
    public class HomeController : Controller
    {
        ImageSubCategoryService _imageSubCategoryService;
        CategoryService _categoryService;
        SubCategoryService _subCategoryService;
        ProductService _productService;
        OrderService _orderService;
        CheckService _checkService;
        CheckItemService _checkItemService;
        AdvertisingService _advertisingService;

        public HomeController(ImageSubCategoryService imageSubCategoryService,CategoryService categoryService, SubCategoryService subCategoryService, 
            ProductService productService, OrderService orderService, CheckService checkService, 
            CheckItemService checkItemService, AdvertisingService advertisingService)
        {
            _imageSubCategoryService = imageSubCategoryService;
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
            _productService = productService;
            _orderService = orderService;
            _checkService = checkService;
            _checkItemService = checkItemService;
            _advertisingService = advertisingService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Orders()
        {
            var viewOrders = (List<ViewOrder>)await GetViewOrders(false);
            return View(viewOrders);
        }

        public async Task<IActionResult> Categories()
        {
            var categories = await _categoryService.GetCategoryList();
            return View(categories);
        }
        public IActionResult AddCategory()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddCategory(Category category, IFormFile titleImage)
        {
            if (ModelState.IsValid && titleImage != null)
            {
                category.ImageBase64 = ConvertImageToBase64String(titleImage);
                await _categoryService.Create(category);
                return RedirectToAction("Categories");
            }
            else
            {
                if (titleImage == null)
                {
                    ModelState.AddModelError("", "Не выбрана картинка");
                }
                return View(category);
            }
        }
        public async Task<IActionResult> EditCategory(int id)
        {
            var category = await _categoryService.GetCategory(id);
            return View(category);
        }
        [HttpPost]
        public async Task<IActionResult> EditCategory(Category category, string oldTitleImage, IFormFile titleImage)
        {
            if (ModelState.IsValid)
            {
                if (titleImage != null)
                    category.ImageBase64 = ConvertImageToBase64String(titleImage);
                else
                    category.ImageBase64 = oldTitleImage;

                await _categoryService.Update(category);
                return RedirectToAction("Categories");
            }
            else
            {
                return View(category);
            }
        }
        public async Task<IActionResult> DeleteCategory(int id)
        {
            await _categoryService.Delete(id);
            return RedirectToAction("Categories");
        }

        public async Task<IActionResult> SubCategories()
        {
            var subCategories = await _subCategoryService.GetSubCategoryList();
            return View(subCategories);
        }
        public async Task<IActionResult> AddSubCategory()
        {
            var categories = await _categoryService.GetCategoryList();
            ViewBag.Categories = categories;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddSubCategory(SubCategory subCategory, IFormFile titleImage, IEnumerable<IFormFile> images)
        {
            if (ModelState.IsValid && titleImage != null)
            {
                subCategory.ImageBase64 = ConvertImageToBase64String(titleImage);
                int subCategoryId = await _subCategoryService.Create(subCategory);
                if (images.Count() != 0)
                {
                    List<ImageSubCategory> imageSubCategories = new List<ImageSubCategory>();
                    foreach (var image in images)
                    {
                        ImageSubCategory imageSubCategory = new ImageSubCategory();
                        imageSubCategory.ImageBase64 = ConvertImageToBase64String(image);
                        imageSubCategory.SubCategoryId = subCategoryId;
                        imageSubCategories.Add(imageSubCategory);
                    }
                    await _imageSubCategoryService.InsertRange(imageSubCategories);
                }
                return RedirectToAction("SubCategories");
            }
            else
            {
                if (titleImage == null)
                {
                    var categories = await _categoryService.GetCategoryList();
                    ViewBag.Categories = categories;
                    ModelState.AddModelError("", "Не выбрана картинка");
                }
                return View(subCategory);
            }
        }
        public async Task<IActionResult> EditSubCategory(int id)
        {
            var categories = await _categoryService.GetCategoryList();
            ViewBag.Categories = categories;
            var subCategory = await _subCategoryService.GetSubCategory(id);
            ViewBag.ImagesSubCategory = await _imageSubCategoryService.GetImageSubCategoryById(id);
            return View(subCategory);
        }
        [HttpPost]
        public async Task<IActionResult> EditSubCategory(SubCategory subCategory, string oldTitleImage, IFormFile titleImage, IEnumerable<IFormFile> images, IEnumerable<int> idsImages)
        {
            if (ModelState.IsValid)
            {
                if (titleImage != null)
                    subCategory.ImageBase64 = ConvertImageToBase64String(titleImage);
                else
                    subCategory.ImageBase64 = oldTitleImage;

                await _subCategoryService.Update(subCategory);
                int subCategoryId = subCategory.Id;
                if (idsImages.Count() != 0)
                {
                    await _imageSubCategoryService.DeleteRange(idsImages);
                }

                if (images.Count() != 0)
                {
                    List<ImageSubCategory> imageSubCategories = new List<ImageSubCategory>();
                    foreach (var image in images)
                    {
                        ImageSubCategory imageSubCategory = new ImageSubCategory();
                        imageSubCategory.ImageBase64 = ConvertImageToBase64String(image);
                        imageSubCategory.SubCategoryId = subCategoryId;
                        imageSubCategories.Add(imageSubCategory);
                    }
                    await _imageSubCategoryService.InsertRange(imageSubCategories);
                }
                return RedirectToAction("SubCategories");
            }
            else
            {
                var categories = await _categoryService.GetCategoryList();
                ViewBag.Categories = categories;
                return View(subCategory);
            }
        }
        public async Task<IActionResult> DeleteSubCategory(int id)
        {
            await _subCategoryService.Delete(id);
            return RedirectToAction("SubCategories");
        }

        public async Task<IActionResult> Products()
        {
            var products = await _productService.GetProductList();
            return View(products);
        }
        public async Task<IActionResult> AddProduct()
        {
            var subCategories = await _subCategoryService.GetSubCategoryList();
            ViewBag.SubCategories = subCategories;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddProduct(Product product)
        {
            if (ModelState.IsValid)
            {
                await _productService.Create(product);
                return RedirectToAction("Products");
            }
            else
            {
                var subCategories = await _subCategoryService.GetSubCategoryList();
                ViewBag.SubCategories = subCategories;
                return View(product);
            }
        }
        public async Task<IActionResult> EditProduct(int id)
        {
            var subCategories = await _subCategoryService.GetSubCategoryList();
            ViewBag.SubCategories = subCategories;
            var product = await _productService.GetProductById(id);
            return View(product);
        }
        [HttpPost]
        public async Task<IActionResult> EditProduct(Product product)
        {
            if (ModelState.IsValid)
            {
                await _productService.Update(product);
                return RedirectToAction("Products");
            }
            else
            {
                var subCategories = await _subCategoryService.GetSubCategoryList();
                ViewBag.SubCategories = subCategories;
                return View(product);
            }
        }
        public async Task<IActionResult> DeleteProduct(int id)
        {
            await _productService.Delete(id);
            return RedirectToAction("Products");
        }

        public async Task<IActionResult> Advertising()
        {
            var advertising = await _advertisingService.GetAdvertisingList();
            return View(advertising);
        }
        public IActionResult AddAdvertising()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddAdvertising(Advertising advertising, IFormFile titleImage)
        {
            if (ModelState.IsValid && titleImage != null)
            {
                advertising.ImageBase64 = ConvertImageToBase64String(titleImage);
                await _advertisingService.Create(advertising);
                return RedirectToAction("Advertising");
            }
            else
            {
                if (titleImage == null)
                {
                    ModelState.AddModelError("", "Не выбрана картинка");
                }
                return View(advertising);
            }
        }
        public async Task<IActionResult> EditAdvertising(int id)
        {
            var advertising = await _advertisingService.GetAdvertising(id);
            return View(advertising);
        }
        [HttpPost]
        public async Task<IActionResult> EditAdvertising(Advertising advertising, string oldTitleImage, IFormFile titleImage)
        {
            if (ModelState.IsValid)
            {
                if (titleImage != null)
                    advertising.ImageBase64 = ConvertImageToBase64String(titleImage);
                else
                    advertising.ImageBase64 = oldTitleImage;

                await _advertisingService.Update(advertising);
                return RedirectToAction("Advertising");
            }
            else
            {
                return View(advertising);
            }
        }
        public async Task<IActionResult> DeleteAdvertising(int id)
        {
            await _advertisingService.Delete(id);
            return RedirectToAction("Advertising");
        }


        public string ConvertImageToBase64String(IFormFile titleImage)
        {
            MemoryStream target = new MemoryStream();
            titleImage.CopyTo(target);
            byte[] data = target.ToArray();
            string base64String = Convert.ToBase64String(data);
            return base64String;
        }

        public async Task<IEnumerable<ViewOrder>> GetViewOrders(bool received)
        {
            List<ViewOrder> viewOrders = new List<ViewOrder>();
            List<Order> orders = (List<Order>)await _orderService.GetOrderList();

            if (orders.Count != 0)
            {
                var checkIdsFromOrders = orders.Select(x => x.CheckId);

                List<Check> checks = (List<Check>)await _checkService.GetChecksByOrderIds(checkIdsFromOrders);
                var checkIds = checks.Select(x => x.Id);

                List<CheckItem> checkItems = (List<CheckItem>)await _checkItemService.GetCheckItemsByCheckIds(checkIds);
                var productIds = checkItems.Select(x => x.ProductId);

                List<Product> products = (List<Product>)await _productService.GetProductsByListIds(productIds);

                foreach (var order in orders)
                {
                    foreach (var check in checks)
                    {
                        if (order.CheckId == check.Id)
                        {
                            List<CheckItem> tmp_checkItems = new List<CheckItem>();
                            List<Product> tmp_products = new List<Product>();

                            foreach (var checkItem in checkItems)
                            {
                                if (check.Id == checkItem.CheckId)
                                {
                                    tmp_checkItems.Add(checkItem);

                                    foreach (var product in products)
                                    {
                                        if (checkItem.ProductId == product.Id)
                                        {
                                            tmp_products.Add(product);
                                            break;
                                        }
                                    }
                                }
                            }

                            ViewOrder viewOrder = new ViewOrder();
                            viewOrder.Order = order;
                            viewOrder.Check = check;
                            viewOrder.CheckItems = tmp_checkItems;
                            viewOrder.Products = tmp_products;

                            viewOrders.Add(viewOrder);
                        }
                    }
                }
            }
            viewOrders.Reverse();
            return viewOrders;
        }



        public IActionResult TestPage()
        {
            //List<Product> products = new List<Product>()
            //{
            //    //new Product(){Name="Фанера ламинированная 1500x3000 мм сорт 1/2 (Брянск)", Price=1560, Discount=0, Dimensions="6мм",  Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    //new Product(){Name="Фанера ламинированная 1500x3000 мм сорт 1/2 (Брянск)", Price=2700, Discount=0, Dimensions="9мм",  Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    //new Product(){Name="Фанера ламинированная 1500x3000 мм сорт 1/2 (Брянск)", Price=2870, Discount=0, Dimensions="12мм", Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    new Product(){Name="Ламинированная стружечная плита 1220х2440 мм", Price=2000, Discount=0, Dimensions="18мм", Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    //new Product(){Name="Ламинированная березовая фанера марки ФОФ F/W (гладкая/сетка) 1500х3000мм", Price=5950, Discount=0, Dimensions="21мм", Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    //new Product(){Name="Ламинированная березовая фанера марки ФОФ F/W (гладкая/сетка) 1220х2440 мм", Price=3400, Discount=0, Dimensions="24мм", Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    //new Product(){Name="Фанера ламинированная 1220х2440мм 2/2 сорт (Жешарт)", Price=3800, Discount=0, Dimensions="24мм", Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //    //new Product(){Name="Фанера ламинированная березовая 1220х2440 мм 1/1 сорт (Вятка)", Price=4400, Discount=0, Dimensions="27мм", Unit = "лист", SubCategoryName = "Fanera_laminirovannaya"},
            //};

            //await _productService.InsertRange(products);

            return View();
        }
    }
}
