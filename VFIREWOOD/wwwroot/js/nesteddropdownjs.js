﻿// Prevent closing from click inside dropdown
$(document).on('click', '.dropdown-menu', function (e) {
    e.stopPropagation();
});

// make it as accordion for smaller screens
$('.dropdown-menu a').on(function (e) {
    e.preventDefault();
    if ($(this).next('.submenu').length) {
        $(this).next('.submenu').toggle();
    }
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.submenu').hide();
    })
});