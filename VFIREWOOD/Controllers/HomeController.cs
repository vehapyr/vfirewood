﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VFIREWOOD.Data;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Infrastructure.Data;
using VFIREWOOD.Models;
using VFIREWOOD.Services.Data;

namespace VFIREWOOD.Controllers
{
    public class HomeController : Controller
    {
        readonly ILogger<HomeController> _logger;
        ApplicationDbContext _db;
        SubCategoryService _subCategoryService;
        AdvertisingService _advertisingService;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext db, 
            IConfiguration configuration,
            SubCategoryService subCategoryService, AdvertisingService advertisingService)
        {
            _logger = logger;
            _db = db;
            _subCategoryService = subCategoryService;
            _advertisingService = advertisingService;
        }

        public async Task<IActionResult> Index()
        {
            var subCategories = await _subCategoryService.GetSubCategoryList();
            var advertisings = await _advertisingService.GetAdvertisingList();
            ViewBag.Advertising = advertisings.Where(x => x.Show).ToList();
            return View(subCategories);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
