﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Services.Data;
using VFIREWOOD.Infrastructure.Business;
using VFIREWOOD.Services.Interfaces.Models;
using Microsoft.AspNetCore.Hosting;

namespace VFIREWOOD.Controllers
{
    public class ProductController : Controller
    {
        private IWebHostEnvironment _env;

        private ImageSubCategoryService _imageSubCategoryService;
        private CategoryService _categoryService;
        private SubCategoryService _subCategoryService;
        private ProductService _productService;
        private CheckItemService _checkItemService;
        private CheckService _checkService;
        private OrderService _orderService;

        public ProductController(IWebHostEnvironment env, ImageSubCategoryService imageSubCategoryService, CategoryService categoryService, 
            SubCategoryService subCategoryService, ProductService productService,
            CheckItemService checkItemService, CheckService checkService, OrderService orderService)
        {
            _env = env;

            _imageSubCategoryService = imageSubCategoryService;
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
            _productService = productService;
            _checkItemService = checkItemService;
            _checkService = checkService;
            _orderService = orderService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult Contacts()
        {
            return View();
        }

        public async Task<ActionResult> Discounts()
        {
            var products = await _productService.GetDiscountedProduct();
            var groupedProducts = products.GroupBy(x => x.Name).ToList();
            return View(groupedProducts);
        }

        [Route("Category/{categoryname:maxlength(50)}")]
        public async Task<ActionResult> Category(string categoryname)
        {
            var category = await _categoryService.GetCategoryByName(categoryname);
            if (category == null)
                return View("Error");
            var subCategories = await _subCategoryService.GetSubCategoryByCategoryName(category.Name);
            ViewBag.CategoryRuName = category.RuName;
            return View(subCategories);
        }

        [Route("Category/{categoryname:maxlength(50)}/{subcategoryname:maxlength(50)}")]
        public async Task<ActionResult> SubCategory(string subcategoryname)
        {
            var subCategory = await _subCategoryService.GetSubCategoryByName(subcategoryname);
            if (subCategory == null)
                return View("Error");
            var products = await _productService.GetProductBySubCategoryName(subcategoryname);
            ViewBag.SubCategory = subCategory;
            ViewBag.ImagesSubCategory = await _imageSubCategoryService.GetImageSubCategoryById(subCategory.Id);
            var groupedProducts = products.GroupBy(x => x.Name).ToList();
            
            return View(groupedProducts);
        }

        [Route("Basket")]
        public async Task<ActionResult> Basket()
        {
            IEnumerable<Product> products = new List<Product>();
            var session = HttpContext.Session.GetString("IdProducts");
            if (session != null)
            {
                List<int> idProducts = JsonConvert.DeserializeObject<List<int>>(session);
                products = await _productService.GetProductsByListIds(idProducts);
            }  
            Response.StatusCode = 200;
            ViewBag.Products = products;
            return View();
        }

        [Route("Basket")]
        [HttpPost]
        public async Task<ActionResult> Basket(string jsonprod, Order order)
        {
            if (ModelState.IsValid)
            {
                OrderProcess orderProcess = new OrderProcess(_productService, _orderService, _checkService, _checkItemService);
                ViewOrder viewOrder = await orderProcess.MakeOrder(jsonprod, order);
                HttpContext.Session.Remove("IdProducts");

                if (viewOrder == null)
                {
                    Response.StatusCode = 400;
                    return View("Error");
                }

                var json = JsonConvert.SerializeObject(viewOrder);
                HttpContext.Session.SetString("viewOrder", json);

                EmailService sendEmail = new EmailService();
                string templatePath = System.IO.Path.Combine(_env.WebRootPath, "Templates");
                sendEmail.SendEmail(viewOrder, order.Email, templatePath);
            }
            else
            {
                return View(order);
            }

            Response.StatusCode = 200;
            return RedirectToAction("YourOrder");
        }

        [HttpPost]
        public async Task<string> PutInBasket(int id)
        {
            var product = await _productService.GetProductById(id);
            if (product != null)
            {
                List<int> idProducts = new List<int>();
                var session = HttpContext.Session.GetString("IdProducts");
                if (session != null)
                    idProducts = JsonConvert.DeserializeObject<List<int>>(session);
                if (!idProducts.Contains(id))
                    idProducts.Add(product.Id);
                var json = JsonConvert.SerializeObject(idProducts);
                HttpContext.Session.SetString("IdProducts", json);
                Response.StatusCode = 200;
                return "in stock";
            }
            Response.StatusCode = 200;
            return "not available";
        }

        [HttpPost]
        public string RemoveProductFromBasket(int? id)
        {
            if (id != null)
            {
                List<int> idProducts = new List<int>();
                idProducts = JsonConvert.DeserializeObject<List<int>>(HttpContext.Session.GetString("IdProducts"));
                idProducts.Remove(id.Value);
                var json = JsonConvert.SerializeObject(idProducts);
                HttpContext.Session.SetString("IdProducts", json);
            }
            Response.StatusCode = 200;
            return "removed";
        }

        [Route("YourOrder")]
        public ActionResult YourOrder()
        {
            if (HttpContext.Session.GetString("viewOrder") != null)
            {
                ViewOrder viewOrder = JsonConvert.DeserializeObject<ViewOrder>(HttpContext.Session.GetString("viewOrder"));
                HttpContext.Session.Remove("viewOrder");
                Response.StatusCode = 200;
                return View(viewOrder);
            }
            Response.StatusCode = 200;
            return RedirectToAction("Index", "Home");
        }
    }
}
