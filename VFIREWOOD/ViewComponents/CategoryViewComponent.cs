﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using VFIREWOOD.Domain.Core;
using VFIREWOOD.Services.Data;

namespace VFIREWOOD.ViewComponents
{
    public class CategoryViewComponent : ViewComponent
    {
        private CategoryService _categoryService;
        private SubCategoryService _subCategoryService;
        public CategoryViewComponent(CategoryService categoryService, SubCategoryService subCategoryService)
        {
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = await _categoryService.GetCategoryList();
            var subCategories = await _subCategoryService.GetSubCategoryList();

            foreach (var cat in categories)
            {
                cat.SubCategories = new List<SubCategory>();
                foreach (var sub in subCategories)
                { 
                    if (cat.Name == sub.CategoryName)
                    {
                        cat.SubCategories.Add(sub);
                    }
                }
            }
            return View(categories);
        }
    }
}
